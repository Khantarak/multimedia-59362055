import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
    selector: 'page-A1',
    templateUrl: 'A1.html'
})
export class A1 {

    constructor(public navCtrl: NavController) {
    }

}