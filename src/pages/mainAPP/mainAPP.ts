import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { A1 } from '../A1/A1';
import { A2 } from '../A2/A2';
import { A3 } from '../A3/A3';
import { A4 } from '../A4/A4';


@Component({
  selector: 'page-mainAPP',
  templateUrl: 'mainAPP.html'
})
export class mainAPP {

  constructor(public navCtrl: NavController) {
  }
  goA1(){
    this.navCtrl.push(A1)
  }
  goA2(){
    this.navCtrl.push(A2)
  }
  goA3(){
    this.navCtrl.push(A3)
  }
  goA4(){
    this.navCtrl.push(A4)
  }
}