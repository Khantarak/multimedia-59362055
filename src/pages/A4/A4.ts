import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { mainAPP } from'../mainAPP/mainAPP';

@Component({
  selector: 'page-A4',
  templateUrl: 'A4.html'
})
export class A4 {

  constructor(public navCtrl: NavController) {
  }

  gomainAPP(){
    this.navCtrl.push(mainAPP)
  }

}