import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { mainAPP } from'../mainAPP/mainAPP';

@Component({
  selector: 'page-A3',
  templateUrl: 'A3.html'
})
export class A3 {

  constructor(public navCtrl: NavController) {
  }

  gomainAPP(){
    this.navCtrl.push(mainAPP)
  }

}