import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { mainAPP } from '../mainAPP/mainAPP';

@Component({
    selector: 'page-A2',
    templateUrl: 'A2.html'
})
export class A2 {

    constructor(public navCtrl: NavController) {
    }

    gomainAPP() {
        this.navCtrl.push(mainAPP)
    }
}