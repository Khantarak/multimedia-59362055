import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';

import { mainAPP } from '../mainAPP/mainAPP';

@Component({
  selector: 'page-Menu',
  templateUrl: 'Menu.html',
})
export class Menu {

  constructor(
    public navCtrl: NavController,
    public loadingCtrl:LoadingController) {
  }

  gomainAPP() {
    this.navCtrl.push(mainAPP);
  }
  goHome(){
    this.navCtrl.parent.select(0);
  }
  goAbout(){
    this.navCtrl.parent.select(1);
  }
  goContact(){
    this.navCtrl.parent.select(2);
  }
  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 1000
    });
    loader.present();
  }
}
